import email
import re
import smtplib
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from time import sleep
from typing import List

from django.conf import settings
from django.utils.encoding import force_text


class SmtpService(object):
    MAX_RETRY = 2
    REQUIRED_CONFIG_FIELDS = ['username', 'password', 'host', 'port']
    FAILED_ERROR_CODE = 454
    AWS_HEADER = "AWS"

    def __init__(
            self,
            config: dict = settings.DEFAULT_EMAIL_CONFIG,
            from_email: str = settings.DEFAULT_FROM_EMAIL
    ):
        # NOTE:
        # for refactoring, we must remove unused parameter reply_email but this is not backward compatible.

        self._validate_email_config(email_config=config)
        self._validate_from_email(from_email=from_email)

        self.provider = config.get('provider')
        self.from_email = from_email
        self.smtp_obj = self._initiate_smtp_obj(config=config)

    def _initiate_smtp_obj(self, config: dict):
        smtp_obj = smtplib.SMTP(host=config['host'], port=config['port'])
        if config.get('tls', True):
            smtp_obj.starttls()
        smtp_obj.login(user=config['username'], password=config['password'])
        return smtp_obj

    def _validate_email_config(self, email_config: dict):
        for required_filed in self.REQUIRED_CONFIG_FIELDS:
            required_filed_value = email_config.get(required_filed)
            if not required_filed_value:
                raise ValueError(f"{required_filed} is not set in email config.")

    def _validate_from_email(self, from_email: str):
        email_regex = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,7}\b'
        if not re.fullmatch(email_regex, from_email):
            raise ValueError("from email is not valid")

    def send_one_email_to_multiple_receivers(
            self,
            subject: str,
            receivers: List[str],
            html_msg: str,
            filepath: str = None,
            filename: str = None,
            quit_service: bool = True
    ):
        # NOTE:
        # for refactoring, we must change not readable parameter q but this is not backward compatible
        # to change parameter name.
        # md was not readable name for metadata and there was no usage for that param in code,
        # So I remove it , but it is not backward compatible too.
        # receiver was expected to be a list in code,
        # So I change its name to receivers, but it is not backward compatible.
        # I change this function name it is more readable, but it is not backward compatible.

        messages = self._create_message_for_each_receiver(
            filename=filename, filepath=filepath, html_msg=html_msg,
            receiver_address_list=receivers, subject=subject
        )

        self._send_messages(messages=messages)

        if quit_service:
            self.close_connection()

    def _create_message_for_each_receiver(
            self,
            subject: str,
            receiver_address_list: List[str],
            html_msg: str,
            filepath: str = None,
            filename: str = None
    ):
        messages = []
        for receiver_address in receiver_address_list:
            messages.append(
                self._create_message(
                    subject=subject,
                    receiver_address=receiver_address,
                    html_msg=html_msg,
                    filepath=filepath,
                    filename=filename

                )
            )
        return messages

    def _create_message(
            self,
            subject: str,
            receiver_address: str,
            html_msg: str,
            filepath: str = None,
            filename: str = None
    ):
        message = self._create_basic_multipart_message(receiver_address=receiver_address, subject=subject)
        if self.provider == self.AWS_HEADER and settings.SES_CONFIGURATION_SET:
            message = self._add_aws_header_to_message(message=message)
        message = self._attach_body_to_message(html_msg=html_msg, message=message)
        if filepath:
            message = self._attach_csv_file_to_message(filename=filename, filepath=filepath, message=message)
        return message

    def _create_basic_multipart_message(self, receiver_address: str, subject: str):
        message = MIMEMultipart()
        message['Subject'] = email.header.Header(force_text(subject), 'utf-8')
        message['From'] = self.from_email
        message['To'] = receiver_address
        return message

    def _add_aws_header_to_message(self, message: MIMEMultipart):
        message['X-SES-CONFIGURATION-SET'] = settings.SES_CONFIGURATION_SET
        return message

    def _attach_body_to_message(self, html_msg: str, message: MIMEMultipart):
        body = MIMEText(html_msg.encode('utf-8'), 'html', 'utf-8')
        message.attach(body)
        return message

    def _attach_csv_file_to_message(self, filename: str, filepath: str, message: MIMEMultipart):
        fp = open(filepath, 'rb')
        attachment_file = MIMEApplication(fp.read(), _subtype="csv")
        fp.close()
        attachment_file.add_header(
            'Content-Disposition',
            'attachment',
            filename=filename
        )
        message.attach(attachment_file)
        return message

    def _send_messages(self, messages: List[MIMEMultipart]):
        for message in messages:
            self._send_message(message)

    def _send_message(self, message: MIMEMultipart):
        for i in range(self.MAX_RETRY):
            try:
                self.smtp_obj.sendmail(
                    from_addr=message['From'], to_addrs=[message['To']], msg=message.as_string()
                )
                break

            except smtplib.SMTPResponseException as error:
                if error.smtp_code == self.FAILED_ERROR_CODE:
                    sleep(1)
                    continue
                raise error

    def close_connection(self):
        self.smtp_obj.quit()
