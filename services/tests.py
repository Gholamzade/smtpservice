import csv
import email
import smtplib
import tempfile
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from smtplib import SMTPResponseException
from unittest.mock import patch

from django.conf import settings
from django.test import TestCase, override_settings
from django.utils.encoding import force_text

from services.smtp import SmtpService


class MockSmtp(smtplib.SMTP):
    def sendmail(self, from_addr, to_addrs, msg, mail_options=(),
                 rcpt_options=()):
        pass

    def quit(self):
        pass


class SMTPServiceTestCase(TestCase):
    def setUp(self):
        self.right_email_config = {
            "username": "test",
            "password": "test",
            "host": "test",
            "port": 123,

        }
        self.email_config_without_username = {
            "password": "test",
            "host": "test",
            "port": 123,

        }
        self.subject = "test"
        self.email_address = "fake@gmail.com"
        self.wrong_email_address = "fake.com"
        self.html_msg = "<h1>Athena</h1>"

    @patch("services.smtp.SmtpService._initiate_smtp_obj", lambda x, config: MockSmtp())
    def _create_smtp_service(self):
        return SmtpService(config=self.right_email_config, from_email=self.email_address)

    def test_create_smtp_service_with_wrong_config_failed(self):
        with self.assertRaises(ValueError):
            SmtpService(
                config=self.email_config_without_username,
                from_email=self.email_address
            )

    def test_create_smtp_service_with_wrong_sender_email_failed(self):
        with self.assertRaises(ValueError):
            SmtpService(
                config=self.right_email_config,
                from_email=self.wrong_email_address
            )

    @patch("services.smtp.SmtpService._initiate_smtp_obj", lambda x, config: MockSmtp())
    def test_create_smtp_service_with_right_data_success(self):
        smtp_service = SmtpService(config=self.right_email_config, from_email=self.email_address)

        self.assertEqual(smtp_service.MAX_RETRY, SmtpService.MAX_RETRY)
        self.assertIsNotNone(smtp_service.smtp_obj)
        self.assertIsNone(smtp_service.provider)

    def _create_basic_multipart_message(self):
        smtp_service = self._create_smtp_service()
        subject = self.subject
        message = smtp_service._create_basic_multipart_message(
            receiver_address=self.email_address, subject=subject
        )
        return message

    def test_create_basic_multipart_message_success(self):
        message = self._create_basic_multipart_message()
        self.assertEqual(type(message), MIMEMultipart)
        self.assertEqual(message['Subject'], email.header.Header(force_text(self.subject), 'utf-8'))
        self.assertEqual(message['From'], self.email_address)
        self.assertEqual(message['To'], self.email_address)

    def test_add_aws_header_to_message_success(self):
        message = self._create_basic_multipart_message()
        smtp_service = self._create_smtp_service()
        new_message = smtp_service._add_aws_header_to_message(message=message)

        self.assertEqual(type(new_message), MIMEMultipart)
        self.assertEqual(message['X-SES-CONFIGURATION-SET'], settings.SES_CONFIGURATION_SET)

    def test_attach_body_to_message_success(self):
        message = self._create_basic_multipart_message()
        smtp_service = self._create_smtp_service()
        new_message = smtp_service._attach_body_to_message(message=message, html_msg=self.html_msg)
        body = MIMEText(self.html_msg.encode('utf-8'), 'html', 'utf-8')
        self.assertEqual(type(new_message), MIMEMultipart)
        self.assertEqual(new_message.get_payload()[0]._payload, body._payload)

    def test_attach_csv_file_to_success(self):
        message = self._create_basic_multipart_message()
        smtp_service = self._create_smtp_service()

        temp_file = self._make_temp_file()
        fp = open(temp_file.name, 'rb')
        attachment_file = MIMEApplication(fp.read(), _subtype="csv")
        attachment_file.add_header(
            'Content-Disposition',
            'attachment',
            filename=temp_file.name
        )

        new_message = smtp_service._attach_csv_file_to_message(
            message=message, filename=temp_file.name, filepath=temp_file.name
        )
        self.assertEqual(type(new_message), MIMEMultipart)
        self.assertEqual(new_message.get_payload()[0]._payload, attachment_file._payload)

    def test_create_message_without_aws_and_pdf_file_success(self):
        smtp_service = self._create_smtp_service()

        message = smtp_service._create_message(
            subject=self.subject, receiver_address=self.email_address, html_msg=self.html_msg
        )
        self.assertEqual(type(message), MIMEMultipart)
        self.assertEqual(message['Subject'], email.header.Header(force_text(self.subject), 'utf-8'))
        self.assertEqual(message['From'], self.email_address)
        self.assertEqual(message['To'], self.email_address)
        self.assertIsNone(message['X-SES-CONFIGURATION-SET'])
        body = MIMEText(self.html_msg.encode('utf-8'), 'html', 'utf-8')
        self.assertEqual(message.get_payload()[0]._payload, body._payload)

    @patch("services.smtp.SmtpService._initiate_smtp_obj", lambda x, config: MockSmtp())
    def test_create_message_without_pdf_file_success(self):
        setting_email_config = settings.DEFAULT_EMAIL_CONFIG
        setting_email_config['provider'] = SmtpService.AWS_HEADER
        with override_settings(DEFAULT_EMAIL_CONFIG=setting_email_config, SES_CONFIGURATION_SET="test"):
            smtp_service = SmtpService()
            message = smtp_service._create_message(
                subject=self.subject, receiver_address=self.email_address, html_msg=self.html_msg
            )
            self.assertEqual(type(message), MIMEMultipart)
            self.assertEqual(message['Subject'], email.header.Header(force_text(self.subject), 'utf-8'))
            self.assertEqual(message['From'], 'xpara2x.ag@gmail.com')
            self.assertEqual(message['To'], self.email_address)
            self.assertEqual(message['X-SES-CONFIGURATION-SET'], "test")
            body = MIMEText(self.html_msg.encode('utf-8'), 'html', 'utf-8')
            self.assertEqual(message.get_payload()[0]._payload, body._payload)

    @patch("services.smtp.SmtpService._initiate_smtp_obj", lambda x, config: MockSmtp())
    def test_create_message_success(self):
        setting_email_config = settings.DEFAULT_EMAIL_CONFIG
        setting_email_config['provider'] = SmtpService.AWS_HEADER
        with override_settings(DEFAULT_EMAIL_CONFIG=setting_email_config, SES_CONFIGURATION_SET="test"):
            smtp_service = SmtpService()
            temp_file = self._make_temp_file()

            fp = open(temp_file.name, 'rb')
            attachment_file = MIMEApplication(fp.read(), _subtype="csv")
            attachment_file.add_header(
                'Content-Disposition',
                'attachment',
                filename=temp_file.name
            )

            message = smtp_service._create_message(
                subject=self.subject, receiver_address=self.email_address, html_msg=self.html_msg,
                filepath=temp_file.name, filename=temp_file.name
            )
            self.assertEqual(type(message), MIMEMultipart)
            self.assertEqual(message['X-SES-CONFIGURATION-SET'], "test")
            body = MIMEText(self.html_msg.encode('utf-8'), 'html', 'utf-8')
            self.assertEqual(message.get_payload()[0]._payload, body._payload)
            self.assertEqual(message.get_payload()[1]._payload, attachment_file._payload)

    def _make_temp_file(self):
        with tempfile.NamedTemporaryFile(mode='w', delete=False) as temp_file:
            csv_writer = csv.writer(temp_file)
            csv_writer.writerow(["a", "b"])
            csv_writer.writerow(["d", "c"])

            temp_file.flush()
            temp_file.seek(0)
        return temp_file

    @patch("services.smtp.SmtpService._initiate_smtp_obj", lambda x, config: MockSmtp())
    def test_create_message_for_each_receiver_success(self):
        setting_email_config = settings.DEFAULT_EMAIL_CONFIG
        setting_email_config['provider'] = SmtpService.AWS_HEADER
        with override_settings(DEFAULT_EMAIL_CONFIG=setting_email_config, SES_CONFIGURATION_SET="test"):
            smtp_service = SmtpService()
            receiver_address_list = [self.email_address]

            temp_file = self._make_temp_file()
            fp = open(temp_file.name, 'rb')
            attachment_file = MIMEApplication(fp.read(), _subtype="csv")
            attachment_file.add_header(
                'Content-Disposition',
                'attachment',
                filename=temp_file.name
            )

            messages = smtp_service._create_message_for_each_receiver(
                subject=self.subject, receiver_address_list=receiver_address_list, html_msg=self.html_msg,
                filepath=temp_file.name, filename=temp_file.name
            )
            message = messages[0]
            self.assertEqual(type(message), MIMEMultipart)
            self.assertEqual(message['X-SES-CONFIGURATION-SET'], "test")
            self.assertEqual(len(messages), len(receiver_address_list))
            body = MIMEText(self.html_msg.encode('utf-8'), 'html', 'utf-8')
            self.assertEqual(message.get_payload()[0]._payload, body._payload)
            self.assertEqual(message.get_payload()[1]._payload, attachment_file._payload)

    @patch("services.tests.MockSmtp.sendmail", side_effect=SMTPResponseException(msg='Test', code=11))
    def test_send_message_raise_smtp_response_error_without_smtp_error_code_failed(self, mock_send_email):
        smtp_service = self._create_smtp_service()
        message = self._create_basic_multipart_message()
        with self.assertRaises(SMTPResponseException):
            smtp_service._send_message(
                message=message
            )

    @patch("services.tests.MockSmtp.sendmail",
           side_effect=SMTPResponseException(msg='Test', code=SmtpService.FAILED_ERROR_CODE))
    def test_send_message_raise_smtp_response_error_with_smtp_error_code_max_retry_call(self, mock_send_email):
        smtp_service = self._create_smtp_service()
        message = self._create_basic_multipart_message()
        smtp_service._send_message(
            message=message
        )
        self.assertEqual(mock_send_email.call_count, SmtpService.MAX_RETRY)

    @patch("services.tests.MockSmtp.sendmail")
    def test_send_message_success_at_first_call(self, mock_send_email):
        mock_send_email.return_value = None
        smtp_service = self._create_smtp_service()
        message = self._create_basic_multipart_message()
        smtp_service._send_message(
            message=message
        )
        self.assertEqual(mock_send_email.call_count, 1)

    @patch("services.tests.MockSmtp.sendmail")
    def test_send_messages_success_at_first_call(self, mock_send_email):
        mock_send_email.return_value = None
        smtp_service = self._create_smtp_service()
        message = self._create_basic_multipart_message()
        messages = [message]
        smtp_service._send_messages(
            messages=messages
        )
        self.assertEqual(mock_send_email.call_count, len(messages))

    @patch("services.tests.MockSmtp.quit")
    @patch("services.tests.MockSmtp.sendmail")
    def test_send_one_email_to_multiple_receivers_with_false_quit_service_param_success(
            self, mock_send_email, mock_quit_smtp):
        mock_send_email.return_value = None
        smtp_service = self._create_smtp_service()
        smtp_service.send_one_email_to_multiple_receivers(
            subject=self.subject, receivers=[self.email_address], html_msg=self.html_msg, quit_service=False
        )
        self.assertEqual(mock_send_email.call_count, 1)
        self.assertEqual(mock_quit_smtp.call_count, 0)

    @patch("services.tests.MockSmtp.quit")
    @patch("services.tests.MockSmtp.sendmail")
    def test_send_one_email_to_multiple_receivers_with_true_quit_service_param_success(self, mock_send_email,
                                                                                       mock_quit_smtp):
        mock_send_email.return_value = None
        smtp_service = self._create_smtp_service()
        smtp_service.send_one_email_to_multiple_receivers(
            subject=self.subject, receivers=[self.email_address], html_msg=self.html_msg, quit_service=True
        )
        self.assertEqual(mock_send_email.call_count, 1)
        self.assertEqual(mock_quit_smtp.call_count, 1)
